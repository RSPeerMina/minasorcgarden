package data;

import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

public class Data {

    public final static Area gardenArea = Area.rectangular(2903, 5463, 2920, 5480);


    public final static Position[][] walkPath = {

            {//winter path
                    new Position(2903,5470),
                    new Position(2900,5476),
                    new Position(2898,5481),
                    new Position(2892,5481),
                    new Position(2892,5484),
                    new Position(2891,5478)

            },


            {//spring Path
                    new Position(2920,5473),
                    new Position(2923,5471),
                    new Position(2923,5466),
                    new Position(2923,5459),
                    new Position(2924,5468),
                    new Position(2928,5470),
                    new Position(2930,5470),
                    new Position(2931,5466),
                    new Position(2935,5473)

            },
            { //autumn path
                    new Position(2908,5461),
                    new Position(2904,5459),
                    new Position(2901,5455),
                    new Position(2901,5451),
                    new Position(2903,5450),
                    new Position(2902,5453),
                    new Position(2908,5456)
            },
            { //summer path
                    new Position(2908, 5482),
                    new Position(2906, 5486),
                    new Position(2906, 5492),
                    new Position(2909, 5490),
                    new Position(2909, 5486),
                    new Position(2914, 5483),
                    new Position(2921, 5485),
                    new Position(2924, 5487),
                    new Position(2917, 5488),
            }

    };

    public final static Area[][][] NpcArea ={
            { //winter npcArea

                    {
                            Area.rectangular(2897,5475,2899,5476),
                            Area.rectangular(2897, 5472, 2897, 5478)
                    },
                    {
                            Area.rectangular(2896,5480,2899,5483),
                            Area.rectangular(2890,5482,2906,5483)
                    },

                    {
                            Area.rectangular(2891,5481,2893,5483),
                            Area.rectangular(2894,5481,2896,5481)
                    },
                    {
                            Area.rectangular(2897, 5485, 2893, 5485)
                    }
            },
            { //spring npcArea

                    {
                            Area.rectangular(2922,5459,2922,5468),
                            Area.rectangular(2922,5471,2922,5466),
                            Area.rectangular(2922,5467,2922,5471)
                    },
                    {
                            Area.rectangular(2925,5459,2926,5461)
                    },
                    {
                            Area.rectangular(2925,5462,2928,5463)
                    },
                    {
                            Area.rectangular(2925,5468,2925,5464),
                            Area.rectangular(2925,5464,2925,5467),
                            Area.rectangular(2925,5464,2925,5465),
                            Area.rectangular(2925,5474,2925,5469),
                            Area.rectangular(2925,5474,2925,5475)
                    },
                    {
                            Area.rectangular(2935,5469,2929,5469),
                            Area.rectangular(2933,5469,2931,5469),
                            Area.rectangular(2930,5469,2930,5469)
                    },
                    {
                            Area.rectangular(2931,5473,2931,5477)
                    }

            },
            { //autumn npcArea
                    {
                            Area.rectangular(2899,5460,2906,5460), //west
                            Area.rectangular(2908,5460,2904,5460) //east
                    },
                    {
                            Area.rectangular(2900,5454,2900,5453), //south
                            Area.rectangular(2900,5454,2900,5451) // north + with next npc

                    },
                    {
                            Area.rectangular(2902,5449,2899,5449) //east

                    },
                    {
                            Area.rectangular(2903,5453,2903,5455),
                            Area.rectangular(2904,5452,2905,5455),
                            Area.rectangular(2904,5454,2905,5454) // or south + next one
                    },
                    {
                            Area.rectangular(2914,5457,2904,5457), // and east
                            Area.rectangular(2903,5457,2908,5457) // west + for herb tree

                    },
                    {
                            Area.rectangular(2915,5455,2911,5455) //and east
                    }

            },
            { //summer npcArea
                    {
                            Area.rectangular(2907, 5484, 2907, 5485) //first mob, north/south
                    },
                    {
                            Area.rectangular(2907, 5492, 2907, 5453) //2nd mob, north/south
                    },
                    {
                            Area.rectangular(2910, 5489, 2910, 5490) //3rd mob, north/south
                    },
                    {
                            Area.rectangular(2913, 5485, 2914, 5485) //4th mob, first run
                    },
                    {
                            Area.rectangular(2918, 5483, 2918, 5485) //4th mob, 2nd run
                    },
                    {
                            Area.rectangular(2923, 5487, 2923, 5489) // 5th mob
                    },
                    {
                            Area.rectangular(2923, 5492, 2923, 5495) // 5th mob
                    }
            }

    };
    public final static Area[][][] playerArea ={

            {       //Winter garden
                    {Area.rectangular(2901,5470,2902,5470)},
                    {Area.rectangular(2899,5476,2901,5476), Area.rectangular(2899,5475,2899,5478)},
                    {Area.rectangular(2900,5479,2896,5483)},
                    {Area.rectangular(2891,5481,2895,5483), Area.rectangular(2891,5485,2894,5485)},
                    {Area.rectangular(2895,5979,2888,5472), Area.rectangular(2889,5481,2888,5484)}
            },
            {
                    //Spring garden
                    {Area.rectangular(2921,5473,2922,5473)},
                    {Area.rectangular(2923,5471,2923,5473)},
                    {Area.rectangular(2923,5465,2923,5466)},
                    {Area.rectangular(2922,5459,2924,5459), Area.rectangular(2923,5459,2923,5458)},
                    {Area.rectangular(2924,5468,2926,5468), Area.rectangular(2925,5470,2925,5466)},
                    {Area.rectangular(2926,5470,2928,5470), Area.rectangular(2928,5470,2928,5468)},
                    {Area.rectangular(2930,5469,2931,5470)}

            },
            {       //Autumn garden
                    {Area.rectangular(2913,5461,2913,5462)},
                    {Area.rectangular(2908,5461,2910,5460)},
                    {Area.rectangular(2904,5459,2904,5460)},
                    {Area.rectangular(2899,5455,2901,5459)},
                    {Area.rectangular(2900,5451,2901,5451)},
                    {Area.rectangular(2903,5449,2903,5451)},
                    {Area.rectangular(2902,5453,2903,5453)},
                    {Area.rectangular(2908,5455,2908,5457)},
                    {Area.rectangular(2909,5448,2917,5453)}

            },
            {       //Summer garden
                    {Area.rectangular(2910, 5481, 2910, 5481)}, //entrance execute step 0
                    {Area.rectangular(2908, 5482, 2908, 5482)}, //before first mob step 1
                    {Area.rectangular(2906, 5486, 2906, 5486)}, //before 2nd mob
                    {Area.rectangular(2906, 5492, 2906, 5492)}, //before 2nd mob step 3
                    {Area.rectangular(2909, 5490, 2909, 5490)}, //before 4th step 4
                    {Area.rectangular(2909, 5486, 2909, 5486)}, //before 8 maze step 5
                    {Area.rectangular(2914, 5483, 2913, 5483)}, //mid 8 maze step 6
                    {Area.rectangular(2921, 5485, 2921, 5485)}, //next to herb step 7
                    {Area.rectangular(2924, 5487, 2924, 5487)} //get reward step 8
            }
    };

    public final static int[] doorID = {
            12617,
            12719,
            12639,
            11987
    };
}
