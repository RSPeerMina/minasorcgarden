package minigame;

import data.Data;
import main.MinaSorcGarden;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.Movement;

public class Walker {
    private MinaSorcGarden S;
    private int GardenId;

    public Walker(MinaSorcGarden s,int gardenId){
        S = s;
        GardenId = gardenId;
    }

    public void pWalk(int p) {
        if (Movement.getRunEnergy() >= Random.low(5, 10) && !Movement.isRunEnabled()) {
            Movement.toggleRun(true);
        }
        if (!S.me.isMoving() && Movement.isRunEnabled()){
            if(Data.walkPath[GardenId][p] != null)
                Movement.walkTo(Data.walkPath[GardenId][p]);
            else{
                Movement.walkTo(Data.walkPath[GardenId][p]);
                Movement.walkTo(Data.walkPath[GardenId][p+1]);
            }
        }
    }
}
