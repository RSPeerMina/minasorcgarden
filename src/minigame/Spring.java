package minigame;

import gui.StartGui;
import main.MinaSorcGarden;
import org.rspeer.runetek.api.commons.Time;

public class Spring {

    private static MinaSorcGarden S;
    private static int east = 1536;
    private static int north = 1024;
    private static int south = 0;
    private static int west = 512;
    private static LoadNpc LNPC;
    private static Walker W;
    private static Reward Rw;

    public static void SpringChecks(MinaSorcGarden s, int step, StartGui sT) {
        S = s;
        W = new Walker(S, sT.selectedGarden());
        LNPC = new LoadNpc(S, sT.selectedGarden());
        Rw = new Reward(s, sT);
        run(step);
    }

    private static void run(int step) {
        switch (step) {

            case 0:
                W.pWalk(1);
                break;

            case 1:
                if (LNPC.npcArea(0, 0)
                        && LNPC.returnNpc(0).getOrientation() == south)
                    W.pWalk(2);
                break;

            case 2:
                if (LNPC.npcArea(0, 2) || LNPC.npcArea(0, 1) && LNPC.returnNpc(0).getOrientation() == north)
                    W.pWalk(3);
                break;

            case 3:
                if (LNPC.returnNpc(3) != null) {
                    if (LNPC.npcArea(1, 0)
                            && LNPC.npcArea(2, 0)
                            && !LNPC.npcArea(3, 4)
                            && LNPC.returnNpc(3).getOrientation() == north)
                        W.pWalk(4);
                }
                break;

            case 4:
                if (((LNPC.npcArea(3, 1)
                        && LNPC.returnNpc(3).getOrientation() == south))
                        || (LNPC.npcArea(3, 2))
                        || (LNPC.npcArea(3, 3)
                        && LNPC.returnNpc(3).getOrientation() == north))
                    W.pWalk(5);

                break;
            case 5:
                if (LNPC.npcArea(4, 0)
                        && LNPC.returnNpc(4).getOrientation() == east) {
                    W.pWalk(6);
                }
                break;

            case 6:
                if ((LNPC.npcArea(5, 0)
                        || LNPC.returnNpc(5).getOrientation() == north)
                        && (LNPC.npcArea(4, 1)
                        && LNPC.returnNpc(4).getOrientation() == east)
                        || (LNPC.npcArea(4, 2)
                        && LNPC.returnNpc(4).getOrientation() == west)) {
                    if (Rw.Reward == 0) {
                        W.pWalk(8);
                        Time.sleep(5000, 6000);
                        Rw.attemptRewardGrab();
                    } else {
                        W.pWalk(7);
                        Time.sleep(3000, 4000);
                        Rw.attemptRewardGrab();
                    }
                }
                break;
        }
    }
}
