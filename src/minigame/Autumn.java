package minigame;

import gui.StartGui;
import main.MinaSorcGarden;

public class Autumn {

    private static MinaSorcGarden S;
    private static int east = 1536;
    private static int north = 1024;
    private static int south = 0;
    private static int west = 512;
    private static int Reward;
    private static LoadNpc LNPC;
    private static Walker W;
    private static Reward Rw;

    public static void AutumnChecks(MinaSorcGarden s, int step, StartGui sT) {
        S = s;
        W = new Walker(S, sT.selectedGarden());
        LNPC = new LoadNpc(S, sT.selectedGarden());
        Rw = new Reward(s, sT);
        Reward = sT.selectedReward();
        run(step);
    }

    private static void run(int step) {
        switch (step) {

            case 0:
                W.pWalk(0);
                break;

            case 1:
                if (LNPC.npcArea(0, 0)
                        && LNPC.returnNpc(0).getOrientation() == west)
                    W.pWalk(1);
                break;

            case 2:
                if (LNPC.npcArea(0, 1)
                        && LNPC.returnNpc(0).getOrientation() == east)
                    W.pWalk(2);
                break;

            case 3:
                if (LNPC.npcArea(1, 0)
                        && LNPC.returnNpc(1).getOrientation() == south)
                    W.pWalk(3);

                break;

            case 4:
                if (LNPC.npcArea(1, 1)
                        && LNPC.returnNpc(1).getOrientation() == north
                        && LNPC.npcArea(2, 0)
                        && LNPC.returnNpc(2).getOrientation() == east)

                    W.pWalk(4);

                break;
            case 5:

                if (LNPC.npcArea(3, 1)
                        || LNPC.npcArea(3, 2)) {

                    W.pWalk(5);
                }

                break;

            case 6:
                if ((LNPC.npcArea(3, 2)
                        || LNPC.returnNpc(3).getOrientation() == south)
                        && LNPC.npcArea(4, 0)
                        && LNPC.returnNpc(4).getOrientation() == east)

                    W.pWalk(6);
                break;

            case 7:
                switch (Reward) {
                    case 0:
                        if (LNPC.npcArea(4, 1)
                                && LNPC.returnNpc(4).getOrientation() == west)

                            Rw.attemptRewardGrab();
                        break;
                    case 1:

                        if (LNPC.npcArea(5, 0)
                                && LNPC.returnNpc(5).getOrientation() == east) {

                            Rw.attemptRewardGrab();
                        }
                        break;
                }
                break;

            case 8:
                Rw.attemptRewardGrab();

        }
    }
}
