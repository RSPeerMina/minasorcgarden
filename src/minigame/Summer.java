package minigame;

import gui.StartGui;
import main.MinaSorcGarden;
import org.rspeer.runetek.api.commons.Time;

public class Summer {

    private static MinaSorcGarden S;
    private static int east = 1536;
    private static int north = 1024;
    private static int south = 0;
    private static int west = 512;
    private static int Reward;
    private static LoadNpc LNPC;
    private static Walker W;
    private static Reward Rw;

    public static void SummerChecks(MinaSorcGarden s, int step, StartGui sT) {
        S = s;
        W = new Walker(S, sT.selectedGarden());
        LNPC = new LoadNpc(S, sT.selectedGarden());
        Rw = new Reward(s, sT);
        Reward = sT.selectedReward();
        run(step);
    }

    private static void run(int step) {
        switch (step) {
            case 0:
                W.pWalk(0); //walk to start position
                break;
            case 1:
                if (LNPC.npcArea(0, 0)
                        && LNPC.returnNpc(0).getOrientation() == north)
                    W.pWalk(1); //walk to first safe spot
                break;
            case 2:
                if (LNPC.npcArea(0, 0)
                        && LNPC.returnNpc(0).getOrientation() == south)
                    W.pWalk(2); //walk to 2nd safe spot, next to 2nd element
                break;
            case 3:
                if (LNPC.npcArea(1, 0)
                        && LNPC.returnNpc(1).getOrientation() == south)
                    W.pWalk(3);
                break;
            case 4:
                if (LNPC.npcArea(2, 0)
                        && LNPC.returnNpc(2).getOrientation() == north)
                    W.pWalk(4);
                break;
            case 5:
                if (LNPC.npcArea(3, 0)
                        && LNPC.returnNpc(3).getOrientation() == east)
                    W.pWalk(5);
                break;
            case 6:
                W.pWalk(6);

                break;
            case 7:
                if (LNPC.returnNpc(4).getOrientation() == north) {

                    W.pWalk(7);
                }
                break;
            case 8:
                switch (Reward) {
                    case 0:
                        if (LNPC.returnNpc(4).getOrientation() == north) {

                            Rw.attemptRewardGrab();
                        }
                        break;
                    case 1:
                        if ((LNPC.returnNpc(4).getOrientation() == south || LNPC.returnNpc(4).getOrientation() == west)
                                && LNPC.returnNpc(4).getPosition().distance() <= 3
                                && (LNPC.returnNpc(5).getOrientation() == north || LNPC.returnNpc(5).getOrientation() == east)) {

                            W.pWalk(8);
                            Time.sleep(3000, 4000);
                            Rw.attemptRewardGrab();
                        }
                        break;
                }
                break;
        }
    }
}
