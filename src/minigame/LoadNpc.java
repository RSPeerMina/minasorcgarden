package minigame;

import main.MinaSorcGarden;
import org.rspeer.runetek.adapter.scene.Npc;
import data.Data;
import org.rspeer.runetek.api.scene.Npcs;

public class LoadNpc {
    private MinaSorcGarden S;
    private int GardenId;
    private Npc[] npcs;

    public LoadNpc(MinaSorcGarden s, int gardenId) {
        S = s;
        GardenId = gardenId;
        npcs = getNPC(gardenId);
    }

    private Npc[] getNPC(int gardenId) {
        Npc[] GardenNpc = npcs;

        if (gardenId == 0) {
            Npc[] gardenNpc = { //winter
                    Npcs.getNearest(5798),
                    Npcs.getNearest(5799),
                    Npcs.getNearest(5800),
                    Npcs.getNearest(5801)
            };
            GardenNpc = gardenNpc;
        } else if (gardenId == 1) {
            Npc[] gardenNpc = { //spring
                    Npcs.getNearest(2956),
                    Npcs.getNearest(2958),
                    Npcs.getNearest(2957),
                    Npcs.getNearest(2962),
                    Npcs.getNearest(2961),
                    Npcs.getNearest(2963),
            };
            GardenNpc = gardenNpc;
        } else if (gardenId == 2) {
            Npc[] gardenNpc = { //autumn
                    Npcs.getNearest(5802),
                    Npcs.getNearest(5803),
                    Npcs.getNearest(5804),
                    Npcs.getNearest(5805),
                    Npcs.getNearest(5806),
                    Npcs.getNearest(5807)

            };
            GardenNpc = gardenNpc;
        } else {
            Npc[] gardenNpc = { //summer
                    Npcs.getNearest(1801),
                    Npcs.getNearest(1802),
                    Npcs.getNearest(1803),
                    Npcs.getNearest(1804),
                    Npcs.getNearest(1805),
                    Npcs.getNearest(1806)
            };
            GardenNpc = gardenNpc;
        }
        return GardenNpc;
    }

    public boolean npcArea(int npc, int step) {

        if (Data.NpcArea[GardenId][npc][step].contains(npcs[npc])) {
            return true;
        }
        return false;
    }

    public Npc returnNpc(int id) {
        return npcs[id];
    }
}
