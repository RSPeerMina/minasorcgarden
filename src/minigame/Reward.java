package minigame;

import main.MinaSorcGarden;
import gui.StartGui;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.SceneObjects;

public class Reward {
    private MinaSorcGarden S;
    public static int GardenId, picked;
    public int Reward;
    public final Area[] rewardArea = {
            Area.rectangular(2888, 5466, 2894, 5479),
            Area.rectangular(2933, 5470, 2935, 5477),
            Area.rectangular(2933, 5470, 2935, 5477),
            Area.rectangular(2913,5487, 2919, 5494)
    };

    public Reward(MinaSorcGarden s, StartGui st) {
        S = s;
        GardenId = st.selectedGarden();
        Reward = st.selectedReward();
    }

    public boolean attemptRewardGrab() {
        switch (Reward) {
            case 0:
                SceneObject herb = SceneObjects.getNearest("Herbs");
                if (herb != null) {
                    pickReward(true);
                    return true;
                }
                break;
            case 1:
                SceneObject tree = SceneObjects.getNearest("Sq'irk tree");
                if (tree != null) {
                    pickReward(true);
                    return true;
                }
                break;
        }
        return false;
    }

    private boolean inRewardZone() {
        if (rewardArea[GardenId].contains(S.me)) {
            return true;
        }
        return false;
    }

    private void pickReward(boolean bypass) {
        if (inRewardZone() || bypass) {
            switch (Reward) {
                case 0:
                    SceneObject herb = SceneObjects.getNearest("Herbs");
                    if (herb != null) {
                        herb.interact("Pick");
                    }
                    break;
                case 1:
                    SceneObject tree = SceneObjects.getNearest("Sq'irk tree");

                    if (tree != null) {
                        tree.interact("Pick-Fruit");
                    }
                    break;
            }
        }
    }

    public static int getPicked() {
        return picked;
    }
}
