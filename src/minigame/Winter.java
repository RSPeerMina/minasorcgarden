package minigame;

import main.MinaSorcGarden;
import data.Data;
import gui.StartGui;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;


public class Winter {
    private static MinaSorcGarden S;
    private static int east = 1536;
    private static Reward Rw;
    private static int Reward;
    private static LoadNpc LNPC;
    private static Walker W;

    public static void WinterChecks(MinaSorcGarden s, int step, StartGui sT) {
        S = s;
        W = new Walker(S, sT.selectedGarden());
        LNPC = new LoadNpc(S, sT.selectedGarden());
        Reward = sT.selectedGarden();
        Rw = new Reward(s, sT);
        run(step);
    }

    private static void run(int step) {
        switch (step) {
            case 0:
                if (LNPC.npcArea(0, 0))
                    W.pWalk(1);
                break;

            case 1:
                if (LNPC.npcArea(0, 1) && LNPC.npcArea(1, 0))
                    W.pWalk(2);
                break;

            case 2:
                if (LNPC.npcArea(2, 1) && LNPC.npcArea(1, 1)) {
                    W.pWalk(4);
                } else if (LNPC.npcArea(2, 0) && LNPC.npcArea(1, 1)) {
                    W.pWalk(3);
                }
                break;

            case 3:

                Movement.walkTo(Data.walkPath[0][4]);

                break;
            case 4:
                int finalx = Random.low(2888, 2890);
                int finaly = Random.low(5472, 5479);

                if (LNPC.npcArea(3, 0) && LNPC.returnNpc(3).getOrientation() == east) {
                    if (!Rw.attemptRewardGrab() && Reward != 1) {
                        Data.walkPath[0][5] = new Position(finalx, finaly, 0);
                        W.pWalk(5);
                    }
                }

                if (!S.me.getPosition().equals(new Position(2892, 5484, 0)))
                    W.pWalk(4);

                break;
        }
    }
}
