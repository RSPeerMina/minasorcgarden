package main;

import data.Data;
import gui.StartGui;
import minigame.Reward;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.event.listeners.ItemTableListener;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.ItemTableEvent;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.net.URL;

import static consumable.Potions.potionID;
import static minigame.Autumn.AutumnChecks;
import static minigame.Spring.SpringChecks;
import static minigame.Summer.SummerChecks;
import static minigame.Winter.WinterChecks;


@ScriptMeta(
        name = "MinaSorcGarden",
        desc = "Does sorc garden",
        developer = "Mina",
        category = ScriptCategory.MINIGAME)

public class MinaSorcGarden extends Script implements RenderListener, ItemTableListener {

    private StopWatch timer;
    private static StartGui startGui;
    public static Player me = Players.getLocal();
    public static boolean shouldBank = false;
    private static boolean oop = false;

    public static Position BANK = new Position(3270, 3167);
    public static Position GARDENPOSITION = new Position(2910, 5471);
    public static Position GARDENENTRANCE = new Position(3320, 3140);
    public static Area FOUNTAIN = Area.rectangular(2910, 5470, 2913, 5473);
    public static Position BANKCHEST = new Position(3308, 3120);

    public void onStart() {
        startGui = new StartGui();
        timer = StopWatch.start();
    }

    public static void banking() {
        if (BANK.distance() >= 400) {
            if (FOUNTAIN.getCenter().distance() <= 20) {
                SceneObject F = SceneObjects.getNearest("Fountain");

                if (F != null) {
                    if (F.isPositionInteractable()) {
                        F.interact("Drink-from");
                    } else {
                        Movement.walkToRandomized(Data.gardenArea.getCenter());
                        Time.sleep(1000, 2000);
                    }

                } else {
                    Movement.walkToRandomized(FOUNTAIN.getCenter());
                    Time.sleep(1000, 2000);
                }
            } else {
                Movement.walkToRandomized(FOUNTAIN.getCenter());
                Time.sleep(1000, 2000);
            }
        } else {
            if (Bank.isOpen()) {
                if (startGui.selectedPot() != 0) {
                    if (!Inventory.contains(potionID)) {
                        Bank.depositAllExcept(potionID);
                        potionBanker();
                    } else {
                        shouldBank = false;
                    }
                } else {
                    Bank.depositInventory();
                }

            } else {
                if (BANKCHEST.distance() <= 9) {
                    SceneObjects.getNearest("Bank chest").interact("Use");
                } else {
                    Movement.walkToRandomized(BANKCHEST);
                    Time.sleep(1000, 2000);
                }
            }
        }
    }

    public static int potionChecker(int start) {
        for (int i = start; i < start + 4; i++) {
            if (Bank.getCount(potionID[i]) >= 3) {
                if (i != start + 4)
                    return potionID[i];
            }
        }

        if (startGui.selectedPot() == 1)
            start = 3;
        else
            start = 7;

        for (int i = start; i > start - 4; i--) {
            if (Inventory.contains(potionID[i])) {

                return potionID[i];
            }
        }

        return 0;
    }


    public static void drink() {
        if (startGui.selectedPot() != 0 && Movement.getRunEnergy() <= Random.low(15, 30) && Data.gardenArea.getCenter().distance() <= 200) {
            int potId = potionChecker(0);
            if (potId != 0 && Inventory.contains(potId) && !me.isMoving()) {
                Inventory.getFirst(potId).interact("Drink");
            }
        }

        if (Inventory.contains("Vial")) {
            Inventory.getFirst("Vial").interact("Drop");
        }
    }

    public void mazeChooser() {
        if (Data.gardenArea.contains(me) && !Inventory.isFull()) {
            openGate();
        }

        switch (startGui.selectedGarden()) {
            case 0:
                for (int i = 0; i <= 4; i++) {
                    if (inMaze(i)) {
                        WinterChecks(this, i, startGui);
                    }
                }
                break;

            case 1:
                for (int i = 0; i <= 6; i++) {
                    if (inMaze(i)) {
                        SpringChecks(this, i, startGui);
                    }
                }

                break;

            case 2:
                for (int i = 0; i <= 8; i++) {
                    if (inMaze(i)) {
                        AutumnChecks(this, i, startGui);
                    }
                }

                break;
            case 3:
                for (int i = 0; i <= 8; i++) {
                    if (inMaze(i)) {
                        SummerChecks(this, i, startGui);
                    }
                }
                break;

        }
        new Reward(this, startGui);
    }

    public static void potionBanker() {
        potionChecker(0);

        if (startGui.selectedPot() != 0) {
            int potId = 0;

            if (startGui.selectedPot() == 1)
                potId = potionChecker(0);
            else
                potId = potionChecker(4);

            Bank.depositAllExcept(3008, 3010, 3012, 3014, 12625, 12627, 12629, 12631);

            if (potId != 0) {
                switch (Inventory.getCount(potId)) {
                    case 0:
                        Bank.withdraw(potId, 4);
                        break;
                    case 1:
                        Bank.withdraw(potId, 3);
                        break;
                    case 2:
                        Bank.withdraw(potId, 2);
                        break;
                    case 3:
                        Bank.withdraw(potId, 1);
                        break;
                    case 4:
                        shouldBank = false;
                        break;
                }
                Time.sleepUntil(() -> Inventory.contains(x -> x.getName().contains("otion")), 1000, 2000);
            }

        }
    }

    public static void returnToGarden() {
        if (GARDENPOSITION.distance() >= 100) {
            if (GARDENENTRANCE.distance() <= 5) {
                Npc APP = Npcs.getNearest("Apprentice");

                if (APP != null && APP.isPositionInteractable()) {
                    APP.interact("Teleport");
                } else {
                    Movement.walkToRandomized(GARDENENTRANCE);
                    Time.sleep(1000, 2000);
                }
            } else {
                Movement.walkToRandomized(GARDENENTRANCE);
                Time.sleep(1000, 2000);
            }
        }
    }


    public int loop() {
        if (startGui.isStarted()) {
            if (Dialog.canContinue()) {
                Dialog.processContinue();
            }

            if (oop) {
                setStopping(true);
            }

            if (startGui.selectedPot() != 0) {
                if (!Inventory.contains(potionID) || Inventory.isFull()) {
                    shouldBank = true;
                    banking();
                } else {
                    if (GARDENPOSITION.distance() >= 200) {
                        returnToGarden();
                    }
                    drink();
                    mazeChooser();
                }
            } else {
                if (GARDENPOSITION.distance() >= 200 && !Inventory.isFull()) {
                    returnToGarden();
                }

                mazeChooser();

                if (Inventory.isFull()) {
                    shouldBank = true;
                }

                if (shouldBank) {
                    banking();
                }
            }
        }

        return 300;
    }

    private boolean inMaze(int id) {
        if (!me.isMoving() && !me.isAnimating()) {
            for (int i = 0; i < Data.playerArea[startGui.selectedGarden()][id].length; i++) {
                if (Data.playerArea[startGui.selectedGarden()][id][i].contains(me)) {
                    if (i > 0) {
                        if (!me.getPosition().equals(Data.walkPath[startGui.selectedGarden()][id])) {
                            Movement.walkTo(Data.walkPath[startGui.selectedGarden()][id]);
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private void openGate() {
        SceneObject door = SceneObjects.getNearest(Data.doorID[startGui.selectedGarden()]);
        if (door != null) {
            door.interact("Open");
            Time.sleep(2000, 4000);
        }
    }

    public void notify(ItemTableEvent e) {
        if (GARDENPOSITION.distance() <= 200) {
            if (e.getChangeType() == ItemTableEvent.ChangeType.ITEM_ADDED) {
                switch (e.getId()) {
                    case 249:
                    case 251:
                    case 253:
                    case 255:
                    case 257:
                    case 259:
                    case 261:
                    case 263:
                    case 265:
                    case 276:
                    case 2481:
                    case 10844:
                    case 10845:
                    case 10846:
                    case 10847:
                        minigame.Reward.picked++;
                        break;
                }
            }
        }
    }

    public void notify(RenderEvent e) {
        Graphics g = e.getSource();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int y = 350;
        int x = 15;
        g2.drawImage(bg, 0, 338, null);
        g2.setColor(Color.CYAN);

        g2.drawString("Runtime: " + timer.toElapsedString(), x, y += 20);
        switch (startGui.selectedGarden()) {
            case 0:
                g2.drawString("Garden: Winter", x, y += 20);
                break;
            case 1:
                g2.drawString("Garden: Spring", x, y += 20);
                break;
            case 2:
                g2.drawString("Garden: Autumn", x, y += 20);
                break;
            case 3:
                g2.drawString("Garden: Summer", x, y += 20);
                break;
        }
        switch (startGui.selectedReward()) {
            case 0:
                g2.drawString("Reward: Herbs", x, y += 20);
                break;
            case 1:
                g2.drawString("Reward: Fruits", x, y += 20);
                break;
        }
        switch (startGui.selectedReward()) {
            case 0:
                g2.drawString("Herbs picked: " + Reward.getPicked() + " (" + (int) timer.getHourlyRate(Reward.getPicked()) + "/h)", x, y += 20);
                break;
            case 1:
                g2.drawString("Fruits picked: " + Reward.getPicked() + " (" + (int) timer.getHourlyRate(Reward.getPicked()) + "/h)", x, y += 20);
                break;
        }

    }

    private final Image bg = getImage("https://i.ibb.co/Dw8w0bv/Screenshot-21.png");

    private Image getImage(String url) {
        try {
            return ImageIO.read(new URL(url));
        } catch (IOException e) {
        }
        return null;
    }

    public void onStop() {
        Log.fine("Thank you for using MinaSorcGarden!");
        super.onStop();
    }
}
